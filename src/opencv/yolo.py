# Usage:
# python yolo.py --video=<path to video file>
# python yolo.py --image=<path to image file>

import numpy as np
import cv2
import argparse
import sys
import numpy as np
import os.path

# Initialize the parameters
confThreshold = 0.5  # Confidence threshold
nmsThreshold = 0.4  # Non-maximum suppression threshold
outputFile = "YOLOv3_output.avi"


def parse_args():
    parser = argparse.ArgumentParser(
        description="Object Detection using YOLO in OPENCV"
    )
    parser.add_argument("--image", help="Path to image file.")
    parser.add_argument("--video", help="Path to video file.")
    args = parser.parse_args()

    # Open the video file
    if not os.path.isfile(args.video):
        print("Input video file ", args.video, " doesn't exist")
        sys.exit(1)

    video = cv2.VideoCapture(args.video)

    # initialise writer to save output
    outputFile = args.video[:-4] + "_output.avi"
    fourcc = cv2.VideoWriter_fourcc("M", "J", "P", "G")
    frameSize = (
        round(video.get(cv2.CAP_PROP_FRAME_WIDTH)),
        round(video.get(cv2.CAP_PROP_FRAME_HEIGHT)),
    )

    vid_writer = cv2.VideoWriter(outputFile, fourcc, 30, frameSize)
    return video


def process_file(cap):
    while cv2.waitKey(1) < 0:
        hasFrame, frame = cap.read()

        if not hasFrame:
            print("File with YOLOv3 output is here :  ", outputFile)
            cv2.waitKey(5000)
            cap.release()
            break


if __name__ == "__main__":
    video = parse_args()

    # object types that we try to detect
    classes = open("./models/YOLOv3/coco.names").read().strip().split("\n")

    # deep neural network
    net = cv2.dnn.readNetFromDarknet(
        "./models/YOLOv3/yolov3.cfg", "./models/YOLOv3/yolov3.model"
    )
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

    process_file(video)
