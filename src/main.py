from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def query_demo():
    options = webdriver.FirefoxOptions()
    options.binary_location = r"C:\PortableApps\Firefox\App\Firefox64\firefox.exe"

    driver = webdriver.Firefox(
        # executable_path=r'"C:\PortableApps\Firefox\geckodriver.exe"',
        options=options,
    )

    driver.get("https://www.apress.com")
    driver.implicitly_wait(5)

    # accept cookies
    cookies_button = driver.find_element(By.CLASS_NAME, "cc-banner__button-accept")
    cookies_button.click()

    # open main menu
    menu = driver.find_element(By.LINK_TEXT, "CATEGORIES")
    ActionChains(driver).move_to_element(menu).pause(5).perform()

    # wait for submenu to show
    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.LINK_TEXT, "Python"))
    )

    python_menu = driver.find_element(By.LINK_TEXT, "Python")
    python_menu.click()

    driver.implicitly_wait(5)

    # copy whole page
    driver.get("https://en.wikipedia.org/wiki/Apress")

    ActionChains(driver).key_down(Keys.CONTROL).send_keys("a").key_up(
        Keys.CONTROL
    ).key_down(Keys.CONTROL).send_keys("c").key_up(Keys.CONTROL).perform()

    # driver.quit()


if __name__ == "__main__":
    print("Launching browser")

    query_demo()

# browser.get('http://www.yahoo.com')
# assert 'Yahoo' in browser.title

# elem = browser.find_element(By.NAME, 'p')  # Find the search box
# elem.send_keys('seleniumhq' + Keys.RETURN)

# browser.quit()
# Example 2:
# Selenium WebDriver is often used as a basis for testing web applications. Here is a simple example using Python’s standard unittest library:

# import unittest
# from selenium import webdriver

# class GoogleTestCase(unittest.TestCase):

#     def setUp(self):
#         self.browser = webdriver.Firefox()
#         self.addCleanup(self.browser.quit)

#     def test_page_title(self):
#         self.browser.get('http://www.google.com')
#         self.assertIn('Google', self.browser.title)

# if __name__ == '__main__':
#     unittest.main(verbosity=2)
